const path = require("path");

module.exports = {
  webpack: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@lib": path.resolve(__dirname, "public/assets/app/lib"),
    },
    output: {
      filename: "[contenthash].bundle.js",
    },
  },
};
