export default class Connection {
  #effectiveTypes = {
    "slow-2g": "slow-2g",
    "2g": "2g",
    "3g": "3g",
    "4g": "4g",
    offline: "offline",
  };

  #getConnectionStatus() {
    const connection =
      navigator?.connection ||
      navigator?.mozConnection ||
      navigator?.webkitConnection;

    return {
      type: connection.type,
      effectiveType: connection.effectiveType,
    };
  }

  #slowEffectiveTypes = [
    this.#effectiveTypes["3g"],
    this.#effectiveTypes["slow-2g"],
    this.#effectiveTypes["2g"],
  ];

  #normalEffectiveType = [this.#effectiveTypes["4g"]];

  isSlowEffectiveTypes(connectionStatus) {
    return this.#slowEffectiveTypes.includes(connectionStatus.effectiveType);
  }

  isNormalEffectiveType(connectionStatus) {
    return this.#normalEffectiveType.includes(connectionStatus.effectiveType);
  }

  isSlowConnectionStatus() {
    return this.isSlowEffectiveTypes(this.#getConnectionStatus());
  }

  isNormalConnectionStatus() {
    return this.isNormalEffectiveType(this.#getConnectionStatus());
  }

  isOffline() {
    return (
      this.#getConnectionStatus().effectiveType === this.#effectiveTypes.offline
    );
  }
}
