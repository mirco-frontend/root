import Connection from "./connection.js";
import { compressed } from "./shared.js";

const connection = new Connection();

export default class Breakpoint {
  constructor(name, mediaQuery, elements) {
    this.name = name;
    this.elements = elements;
    this.breakpoint = window.matchMedia(mediaQuery);
    this.handler = this.handler.bind(this);
    this.start = this.start.bind(this);
    this.stop = this.stop.bind(this);
    this.init = this.init.bind(this);
  }

  init() {
    this.handler(this.breakpoint);
  }

  handler(event) {
    const controller = navigator.serviceWorker.controller;

    this.elements.forEach((element) => {
      if (controller === null) {
        if (connection.isSlowConnectionStatus()) {
          element.addClasses(compressed["1x"]);
        } else {
          element.removeClasses(compressed["1x"]);
        }
        if (connection.isNormalConnectionStatus()) {
          element.addClasses(compressed.no);
        } else {
          element.removeClasses(compressed.no);
        }
      }
      if (event.matches) {
        element.addClasses(this.name);
      } else {
        element.removeClasses(this.name);
      }
    });
  }

  start() {
    this.breakpoint.addEventListener("change", this.handler);

    return this.stop;
  }
  stop() {
    this.breakpoint.removeEventListener("change", this.handler);
  }
}
