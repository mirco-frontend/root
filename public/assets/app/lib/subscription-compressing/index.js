import Element from "./element.js";
import Breakpoint from "./breakpoint.js";
import { breakpoints } from "./shared.js";

const elements = [new Element(document.body)];

const desktop = new Breakpoint(
  "desktop",
  `(min-width: ${breakpoints.xl})`,
  elements
);

const laptop = new Breakpoint(
  "laptop",
  `(min-width : ${breakpoints.lg}) and (max-width: ${breakpoints.xl})`,
  elements
);

const tablet = new Breakpoint(
  "tablet",
  `(min-width : ${breakpoints.sm}) and (max-width: ${breakpoints.lg})`,
  elements
);

const mobile = new Breakpoint(
  "mobile",
  `(min-width: ${breakpoints.xs}) and (max-width : ${breakpoints.sm})`,
  elements
);

desktop.init();
laptop.init();
tablet.init();
mobile.init();

desktop.start();
laptop.start();
tablet.start();
mobile.start();
