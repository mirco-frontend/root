export default class Element {
  constructor(element) {
    this.element = element;
  }

  containsClass = (token) => this.element.classList.contains(token);

  addClasses = (...tokens) => {
    this.element.classList.add(...tokens);
  };

  removeClasses = (...tokens) => {
    this.element.classList.remove(...tokens);
  };
}
