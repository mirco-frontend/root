export const compressed = {
  no: "no-compressed",
  "1x": "compressed-1x",
};

export const breakpoints = {
  xs: 0,
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px",
  xxl: "1400px",
};
