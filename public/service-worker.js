/* eslint-disable no-restricted-globals */
import Connection from "./assets/app/lib/subscription-compressing/connection.js";
import { compressed } from "./assets/app/lib/subscription-compressing/shared.js";

const connection = new Connection();

const CACHE_NAME = "v1";

function isCompressed1x(url) {
  return url.includes(compressed["1x"]);
}
function isNoCompressed(url) {
  return url.includes(compressed.no);
}
function toCompressed1x(url) {
  return url.replace(compressed.no, compressed["1x"]);
}
function toNoCompressed(url) {
  return url.replace(compressed["1x"], compressed.no);
}

self.addEventListener("install", (event) => {
  console.log("Установлен", event);
  self.skipWaiting();
});

self.addEventListener("activate", async (event) => {
  console.log("Активирован", event);

  const cacheNames = await caches.keys();

  await Promise.all(
    cacheNames.map(async (cacheName) => {
      if (cacheName !== CACHE_NAME) {
        await caches.delete(cacheName);
      }
    })
  );

  event.waitUntil(self.clients.claim());
});

function isImage(evt) {
  return evt.request.method === "GET" && evt.request.destination === "image";
}

self.addEventListener("fetch", (evt) => {
  return evt.respondWith(
    (async () => {
      if (isImage(evt)) {
        const url = new URL(evt.request.url);

        const pathname = url.pathname;

        if (connection.isSlowConnectionStatus()) {
          const isNoCompressedImage = isNoCompressed(pathname);

          if (isNoCompressedImage) {
            const noCompressedImageInCache = await caches.match(pathname);

            const compressed1xImagePathname = toCompressed1x(pathname);

            const compressed1xImageInCache = await caches.match(
              compressed1xImagePathname
            );

            if (!noCompressedImageInCache && !compressed1xImageInCache) {
              const cachedResponseImage = await fetch(
                compressed1xImagePathname
              );

              const cache = await caches.open(CACHE_NAME);

              await cache.put(
                compressed1xImagePathname,
                cachedResponseImage.clone()
              );

              return cachedResponseImage;
            } else {
              const cachedResponseImage =
                noCompressedImageInCache || compressed1xImageInCache;

              return cachedResponseImage;
            }
          }
        }
        if (connection.isOffline()) {
          if (isCompressed1x(pathname) || isNoCompressed(pathname)) {
            const cachedResponseCompressed1x = await caches.match(
              toCompressed1x(pathname)
            );
            const cachedResponseNoCompressed = await caches.match(
              toNoCompressed(pathname)
            );

            return cachedResponseNoCompressed || cachedResponseCompressed1x;
          }
        }

        const cachedResponse = await caches.match(evt.request);

        if (cachedResponse) {
          return cachedResponse;
        }

        const response = await fetch(evt.request);

        const cache = await caches.open(CACHE_NAME);

        await cache.put(evt.request, response.clone());

        return response;
      }

      return await fetch(evt.request);
    })()
  );
});
